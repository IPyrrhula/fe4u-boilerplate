export default function filterUsers(users, country, age, gender, favorite) {
  return users.filter((user) => {
    const filterByCountry = country === null || user.country === country;
    const filterByAge = age === null || user.age === age;
    const filterByGender = gender === null || user.gender === gender;
    const filterByFavourite = favorite === null || user.favorite === favorite;

    return filterByCountry && filterByAge && filterByGender && filterByFavourite;
  });
}
