import crypto from 'crypto';
import { additionalUsers } from './FE4U-Lab3-mock.js';

const templateUser = {
  gender: null,
  title: null,
  full_name: null,
  city: null,
  state: null,
  country: null,
  postcode: null,
  coordinates: null,
  timezone: null,
  email: null,
  b_day: null,
  age: null,
  phone: null,
  picture_large: null,
  picture_thumbnail: null,
  id: null,
  favorite: null,
  course: null,
  bg_color: null,
  note: null,
};

function generateUniqueID() {
  const usedIDs = new Set(additionalUsers.map((user) => user.id));
  let id = crypto.randomBytes(Math.floor(Math.random() * 6) + 6);
  while (usedIDs.has(id)) {
    id = crypto.randomBytes(Math.floor(Math.random() * 8) + 12);
  }
  return id.toString('hex');
}

function getRandomCourse() {
  const courses = ['Mathematics', 'Physics', 'English', 'Computer Science', 'Dancing', 'Chess', 'Biology', 'Chemistry', 'Law', 'Art', 'Medicine', 'Statistics'];
  return courses[Math.floor(Math.random() * courses.length)];
}

function getRandomBgColor() {
  const randomColor = Math.floor(Math.random() * 16777215).toString(16);
  return `#${randomColor}`;
}

function getRandomFavorite() {
  return Math.random() < 0.5;
}

function formatObjectsToTemplate(users) {
  return users.map((user) => ({
    gender: user.gender,
    title: user.name.title,
    full_name: `${user.name.first} ${user.name.last}`,
    city: user.location.city,
    state: user.location.state,
    country: user.location.country,
    postcode: user.location.postcode,
    coordinates: user.location.coordinates,
    timezone: user.location.timezone,
    email: user.email,
    b_day: user.dob.date,
    age: user.dob.age,
    phone: user.phone,
    picture_large: user.picture.large,
    picture_thumbnail: user.picture.thumbnail,
    id: generateUniqueID(),
    favorite: getRandomFavorite(),
    course: getRandomCourse(),
    bg_color: getRandomBgColor(),
    note: "Here's gonna be teacher's note",
  }));
}

function fillMissingFields(object, template) {
  return object.map((user) => ({ ...template, ...user }));
}
export default function mergeObjects(usersMock, extraUsers) {
  const formattedUsersMock = formatObjectsToTemplate(usersMock);
  const formattedAdditionalUsers = fillMissingFields(extraUsers, templateUser);
  return [
    ...formattedUsersMock,
    ...formattedAdditionalUsers
      .filter((user) => !formattedUsersMock
        .some((existingObj) => (!user.email || existingObj.email === user.email)
              && existingObj.full_name === user.full_name)),
  ];
}
