export default function searchUsers(users, paramType, param) {
  const validParamTypes = ['full_name', 'note', 'age'];
  if (!validParamTypes.includes(paramType)) {
    throw new Error('Invalid param type!');
  }
  return users.filter((user) => user[paramType] === param);
}
