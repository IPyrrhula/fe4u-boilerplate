export default function sortUsers(users, param, byAscending = true) {
  const validParams = ['full_name', 'age', 'b_day', 'country'];
  if (!validParams.includes(param)) {
    throw new Error('Invalid parameter!');
  }
  return users.sort((userPrev, userNext) => {
    if (userPrev[param] > userNext[param]) {
      return byAscending ? 1 : -1;
    }
    if (userPrev[param] < userNext[param]) {
      return byAscending ? -1 : 1;
    }
    return 0;
  });
}
