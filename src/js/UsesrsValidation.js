import { isValidNumberForRegion } from 'libphonenumber-js';
import { lookup } from 'country-data';

function isUpper(letter) {
  return letter === letter.toUpperCase();
}
function isValidString(variable) {
  return typeof variable === 'string' && isUpper(variable.charAt(0));
}
function isNumber(variable) {
  return typeof variable === 'number';
}
function isValidPhone(phone, countryName) {
  const countryCode = lookup.countries({ name: countryName })[0].alpha2;
  return isValidNumberForRegion(phone, countryCode);
}
function isValidEmail(email) {
  const emailRegex = /^((?!\.)[\w-_.]*[^.])(@\w+)(\.\w+(\.\w+)?[^.\W])$/gm;
  return emailRegex.test(email);
}

function validateStringFields(array) {
  return array.reduce((isValid, variable) => isValid && isValidString(variable), true);
}
export default function validateUser(user) {
  const fields = [user.full_name, user.note, user.state, user.city, user.country];
  const isValid = validateStringFields(fields) && isNumber(user.age) && isValidPhone(user.phone, user.country) && isValidEmail(user.email);
  if (!isValid) console.log(`${user.full_name} didn't pass the validation`);
  else console.log(`${user.full_name} passed the validation`);
}
