import { randomUserMock, additionalUsers } from './FE4U-Lab3-mock.js';
import mergeObjects from './FromatUsers.js';
import validateUser from './UsesrsValidation.js';
import filterUsers from './FilterUsers.js';
import sortUsers from './SortUsers.js';
import searchUsers from './SearchUsers.js';
import getPercentage from './UsersPercentage.js';

// Task 1
const users = mergeObjects(randomUserMock, additionalUsers);
console.log(users);

// Task 2
users.forEach((user) => validateUser(user));

// Task 3
console.log(filterUsers(users, 'Germany', 50, 'female', null));

// Task 4
console.log(sortUsers(users, 'country'));

// Task 5
console.log(searchUsers(users, 'age', '47'));

// Task 6
console.log(getPercentage(users, 'age', 47));
